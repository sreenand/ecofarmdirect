package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;

/**
 * Created by srinandchallur on 06/04/16.
 */
public class OrderHistoryAdapter extends BaseAdapter {

    private List<Order> orderList;
    private Activity activity;
    private SimpleDateFormat sdf;

    //new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz")

    public OrderHistoryAdapter(List<Order> orderList, Activity activity){
        this.orderList = orderList;
        this.activity = activity;
        //sdf = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        sdf = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm a");
    }

    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orderList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Order order = orderList.get(position);
        View view = convertView;
        if (view == null)
            view = activity.getLayoutInflater().inflate(R.layout.order_history_row, null);
        ((TextView)(view.findViewById(R.id.orderId))).setText(order.getId().toString());
        ((TextView)(view.findViewById(R.id.orderDate))).setText(sdf.format(order.getOrderDate()));
        ((TextView)(view.findViewById(R.id.orderStatus))).setText(order.getStatus().toString());

        if(order.getOrderPayment() != null) {
            ((TextView) (view.findViewById(R.id.paymentStatus))).setText(order.getOrderPayment().getStatus());
        }


        if(order.getPriceCalculated() != null){
            ((TextView)(view.findViewById(R.id.orderAmount))).setText("\u20B9" + order.getPriceCalculated().toString());
        } else if (order.getPricePromised() != null){
            ((TextView)(view.findViewById(R.id.orderAmount))).setText("\u20B9" + order.getPricePromised());
        } else{
            ((TextView)(view.findViewById(R.id.orderAmount))).setText("\u20B9" + "0.0");
        }
        return view;
    }


}
