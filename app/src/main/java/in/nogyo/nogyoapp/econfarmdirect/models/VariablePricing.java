package in.nogyo.nogyoapp.econfarmdirect.models;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by hariprasanthac on 18/07/16.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class VariablePricing extends AbstractTimeStamp{
    public Long productPricingId;
    public int bucketId;
    public Double min;
    public Double max;
    public Double discountedPrice;
}
