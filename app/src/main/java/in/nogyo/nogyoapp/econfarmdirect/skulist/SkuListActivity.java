package in.nogyo.nogyoapp.econfarmdirect.skulist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.checkout.CheckoutActivity;
import in.nogyo.nogyoapp.econfarmdirect.main.MainActivity;
import in.nogyo.nogyoapp.econfarmdirect.menu.BaseActivityWithMenu;
import in.nogyo.nogyoapp.econfarmdirect.models.Product;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import in.nogyo.nogyoapp.econfarmdirect.models.SubCategory;
import in.nogyo.nogyoapp.econfarmdirect.rest.HttpRequest;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter @Setter @NoArgsConstructor
public class SkuListActivity extends BaseActivityWithMenu {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SubCategoryPagerAdapter mSubCategoryPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String seletedLanguage;
    private Map<SubCategory,List<ProductPricing>> populatedProducts = new ConcurrentHashMap<>();
    private Set<ProductPricing> selectedItems;
    private List<SubCategory> subCategoryList;
    private long mBackPressed;
    private static final int TIME_INTERVAL = 2000;
    private ViewPager mViewPager;
    private EcoFarmContext context;


    public SkuListActivity getCurrentActivity(){
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skulist);
        createMenu();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        selectedItems = new HashSet<>();

        try {
            context = (EcoFarmContext) getApplicationContext();
            if(context.getCart() == null){
                context.setCart(new HashMap<Long, ProductPricing>());
            }
            HttpRequest task = new HttpRequest(Constants.backend_sku_url+"/category/1/subcategories","GET",null,null,null,
                    0,context.getSessionUser().getSessionId(),null){

                List<SubCategoryFragment> fragmentList = new ArrayList<>();
                @Override
                protected void onPostExecute(Response response){

                    try {
                        subCategoryList = RestUtil.mapper.readValue(response.getResponseBody(), new TypeReference<List<SubCategory>>(){});
                        for(SubCategory subCategory : subCategoryList){
                            subCategory.setLocalNamesMap(RestUtil.mapper.readValue(subCategory.getLocalNames(),HashMap.class));
                            fragmentList.add(new SubCategoryFragment(subCategory,getCurrentActivity()));
                        }
                    } catch (Exception e) {
                        Log.e("ERROR","UNABLE to get Sub category List",e);
                    }


                    mSubCategoryPagerAdapter = new SubCategoryPagerAdapter(getSupportFragmentManager(),fragmentList);
                    mViewPager = (ViewPager) findViewById(R.id.container);
                    mViewPager.setAdapter(mSubCategoryPagerAdapter);
                    TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(mViewPager);

                }
            };
            task.execute((Void)null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((ImageView)(findViewById(R.id.titleImage))).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(v);
            }
        });
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu_button);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void logOut(){
        try {
            context.setSessionUser(null);
            context.setStore(null);
            context.setSelectedOrder(null);
            context.setStoreCity(null);
            Thread.sleep(1000);
            startActivity(CommonUtils.navigate(SkuListActivity.this, MainActivity.class));
        } catch (InterruptedException e) {
            Log.e("ERROR", "ERROR in thread wait");

        }
        CommonUtils.clearSessionData(this);
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()){
            this.context.persistCart();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        } else {
            Toast.makeText(getBaseContext(), "Tap back button in order to exit", Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_skulist, menu);
        return true;
    }

    @Override
    public void displayView(View view) {
        super.displayView(view);
    }


    public void refresh(View view) {
        this.startActivity(CommonUtils.navigate(SkuListActivity.this, SkuListActivity.class));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;

            case R.id.action_checkout:


                if(context.getCart() == null || context.getCart().size() == 0){
                    Toast.makeText(this, R.string.please_select_something, Toast.LENGTH_SHORT).show();
                    return false;
                }

                try {
                    Intent intent = CommonUtils.navigate(SkuListActivity.this, CheckoutActivity.class);
                    startActivity(intent);
                } catch (Exception e){
                    Log.e("ERROR","Error in serializing cart",e);
                }

                break;
            default:
                break;
        }

        return true;
    }

    @Getter @Setter @NoArgsConstructor
    public static class SubCategoryFragment extends Fragment {
        private SubCategory subCategory;
        private SkuListActivity skuListActivity;
        private LinearLayout fragmentListLayout;

        public SubCategoryFragment(SubCategory subCategory, SkuListActivity activity) {
            this.subCategory = subCategory;
            this.skuListActivity = activity;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_skulist, container, false);
            fragmentListLayout = (LinearLayout)rootView.findViewById(R.id.sku_list);
            SKUFragmentPopulateTask task = new SKUFragmentPopulateTask(this,skuListActivity);
            task.execute((Void)null);
            return rootView;
        }
    }


    @Getter @Setter
    public class SubCategoryPagerAdapter extends FragmentPagerAdapter {

        private final List<SubCategoryFragment> subCategoryFragmentList = new ArrayList<>();

        public SubCategoryPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public SubCategoryPagerAdapter(FragmentManager fm, List<SubCategoryFragment> fragmentList) {
            super(fm);
            this.subCategoryFragmentList.addAll(fragmentList);
        }

        @Override
        public Fragment getItem(int position) {
            return subCategoryFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return subCategoryFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return subCategoryFragmentList.get(position).getSubCategory().getLocalName(context.getSelectedLanguage());
        }
    }
}
