package in.nogyo.nogyoapp.econfarmdirect.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 27/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class Locality {


    private Long id;
    private String name;
    private City city;
    private Long cityId;
    private boolean serviceable;
    private String pincode;
    private Double lattitudeSouth;
    private Double lattitudeNorth;
    private Double longitudeEast;
    private Double longitudeWest;
}
