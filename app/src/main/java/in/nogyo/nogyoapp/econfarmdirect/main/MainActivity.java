package in.nogyo.nogyoapp.econfarmdirect.main;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.login.LoginActivity;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.rest.HttpRequest;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import in.nogyo.nogyoapp.econfarmdirect.skulist.SkuListActivity;

public class MainActivity extends AppCompatActivity {

    private EcoFarmContext context;

    public MainActivity getCurrentActivity(){
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            getSupportActionBar().hide();

            context = (EcoFarmContext)getApplicationContext();
            VersionCheckTask task = new VersionCheckTask(this){

                @Override
                protected void onPostExecute(Response response){
                    this.onpostExecute(response);

                    if(!context.isLaunchPlayStore()){
                        Map<String,String> data = (Map<String,String>)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll();
                        final String existingSessionId = data.get(Constants.K3);

                        if(existingSessionId == null){
                            startActivity(CommonUtils.navigate(MainActivity.this, LoginActivity.class));
                            MainActivity.this.finish();
                            return;
                        }

                        HttpRequest request = new HttpRequest(Constants.backend_tick_url,"GET",null,null,null,0,existingSessionId,null){

                            protected void onPostExecute(Response result){
                                ((ProgressBar)findViewById(R.id.titleImageProgressBar)).setVisibility(View.GONE);
                                if(result.getResponseCode() != 200){
                                    startActivity(CommonUtils.navigate(MainActivity.this, LoginActivity.class));
                                    MainActivity.this.finish();
                                    return;

                                } else{
                                    try {
                                        User user = RestUtil.mapper.readValue(result.getResponseBody(),User.class);
                                        user.setSessionId(existingSessionId);
                                        context.setSessionUser(user);

                                        HttpRequest request = new HttpRequest(Constants.backend_mystores_url,"GET",null,null,null,100,this.getSessionId(),null){
                                            protected void onPostExecute(Response result){
                                                try {
                                                    List<Store> storeList = RestUtil.mapper.readValue(result.getResponseBody(), new TypeReference<List<Store>>() {});
                                                    context.setStore(storeList.get(0));
                                                    context.setStoreCity(storeList.get(0).getLocality().getCity());
                                                    startActivity(CommonUtils.navigate(MainActivity.this, SkuListActivity.class));
                                                } catch (IOException e) {
                                                    Log.e("ERROR","Unable to set the store",e);
                                                    return;
                                                }
                                            }
                                        };
                                        request.execute((Void) null);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }


                                }

                            }
                        };
                        request.execute((Void) null);
                    }


                }

            };

            task.execute((Void) null);
            context.initializeCart(this);
        } catch (Exception e){
            Log.e("ERROR","Error in main Activity",e);
        }
    }
}
