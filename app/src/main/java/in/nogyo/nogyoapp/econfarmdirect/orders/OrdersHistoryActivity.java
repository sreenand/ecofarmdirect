package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrdersHistoryActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private ListView listView;
    private List<Order> orderList;
    private OrderHistoryAdapter orderHistoryAdapter;
    private boolean fetchData = true;
    private boolean loadingMore = false;
    private Store store;
    private TextView noOrdersText;
    private EcoFarmContext context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_orders_history);
            context  =(EcoFarmContext)getApplicationContext();
            listView = (ListView)findViewById(R.id.ordersList);
            this.orderList = new ArrayList<>();
            progressBar = (ProgressBar)findViewById(R.id.orderHistoryProgress);
            noOrdersText = (TextView)findViewById(R.id.no_orders_text);
            this.store = context.getStore();
            listView.setOnScrollListener(new OrderHistoryScrollChangeListener(this));
            this.orderHistoryAdapter = new OrderHistoryAdapter(orderList,this);
            listView.setAdapter(orderHistoryAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position,
                                        long id) {

                    Order order = (Order) parent.getAdapter().getItem(position);
                    try {
                        context.setSelectedOrder(order);
                        Intent intent = CommonUtils.navigate(OrdersHistoryActivity.this, OrderActivity.class);
                        intent.putExtra(Constants.CALLER_ACTIVITY, Constants.ACTIVITY_ORDER_HISTORY);
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), R.string.unable_to_select_store, Toast.LENGTH_LONG).show();
                        Log.v("SCHEMA", "onItemClick Nuked!");
                    }
                }
            });
//            OrderHistoryPopulateTask task = new OrderHistoryPopulateTask(store.getId(),30,0,this);
//            task.execute((Void) null);


            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        } catch (Exception e) {
            Log.e("ERROR","Something went wrong terribly",e);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
