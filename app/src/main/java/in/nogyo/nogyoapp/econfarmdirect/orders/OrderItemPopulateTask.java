package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.OrderItem;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 08/04/16.
 */

@Getter @Setter
public class OrderItemPopulateTask extends AsyncTask<Void, Void, Boolean> {


    private int limit;
    private int offset;
    private OrderActivity activity;
    private EcoFarmContext context;
    private List<OrderItem> orderItemList;


    public OrderItemPopulateTask(int limit, int offset, OrderActivity activity, List<OrderItem> orderItemList){
        this.offset = offset;
        this.limit = limit;
        this.activity = activity;
        this.orderItemList = orderItemList;
        this.context = (EcoFarmContext)this.activity.getApplicationContext();
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        activity.setLoadingMore(true);
        orderItemList = OrderUtil.getOrderItemsForOrder(context.getSelectedOrder().getId(),limit,offset,activity);
        activity.getOrderItemList().addAll(getOrderItemList());
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        activity.getOrderItemListAdapter().notifyDataSetChanged();
        activity.setLoadingMore(false);
        if(getOrderItemList().size() < 10){
            activity.setFetchData(false);
            ((TextView) activity.findViewById(R.id.items_text)).setText("Items : " + activity.getOrderItemList().size());
        }
    }
}
