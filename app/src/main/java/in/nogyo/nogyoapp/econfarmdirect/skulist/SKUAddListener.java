package in.nogyo.nogyoapp.econfarmdirect.skulist;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import java.util.Map;
import java.util.Set;


import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 24/02/16.
 */

@Getter @Setter
public class SKUAddListener implements CompoundButton.OnCheckedChangeListener {

    private ProductPricing product;
    private Set<ProductPricing> selectedItems;
    private EditText quantity;
    private EcoFarmContext context;

    public SKUAddListener(ProductPricing product, Set<ProductPricing> selectedItems,EditText quantity, EcoFarmContext context){
        this.product = product;
        this.quantity = quantity;
        this.selectedItems = selectedItems;
        this.context = context;
    }


    public void onClick(View view) {
        Switch button = (Switch)view.findViewById(R.id.groceryAdd);
        int quantity = -1;
        try{
            quantity = Integer.parseInt(this.quantity.getText().toString());
        }catch (Exception e){

        }
        if(button.isChecked()){
            if(quantity <= 0){
                button.setChecked(false);
                return;
            }
            product.getProduct().setSelected(true);
            this.product.getProduct().setQuantity(quantity);
            this.quantity.setNextFocusDownId(R.id.groceryQuantity);
            this.quantity.setBackgroundResource(R.drawable.no_edittext_style);
            this.quantity.setEnabled(false);
            this.selectedItems.add(product);
        } else if(!button.isChecked()){
            this.quantity.setFocusable(true);
            this.quantity.requestFocus();
            this.quantity.setBackgroundResource(R.drawable.edittext_style);
            product.getProduct().setSelected(false);
            this.quantity.setEnabled(true);
            this.quantity.setText("");
            this.selectedItems.remove(product);
            this.getProduct().getProduct().setQuantity(0);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        int quantity = -1;
        try{
            quantity = Integer.parseInt(this.quantity.getText().toString());
        }catch (Exception e){

        }

        if(isChecked){
            if(quantity <= 0){
                buttonView.setChecked(false);
                return;
            }
            product.getProduct().setSelected(true);
            this.product.getProduct().setQuantity(quantity);
            this.quantity.setNextFocusDownId(R.id.groceryQuantity);
            this.quantity.setBackgroundResource(R.drawable.no_edittext_style);
            this.quantity.setEnabled(false);
            this.selectedItems.add(product);
            context.addToCart(product);


        } else {

            this.quantity.setFocusable(true);
            this.quantity.requestFocus();
            this.quantity.setBackgroundResource(R.drawable.edittext_style);
            product.getProduct().setSelected(false);
            this.quantity.setEnabled(true);
            this.quantity.setText("");
            this.selectedItems.remove(product);
            this.getProduct().getProduct().setQuantity(0);
            context.removeFromCart(product);

        }

    }
}
