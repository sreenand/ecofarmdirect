package in.nogyo.nogyoapp.econfarmdirect.models;

/**
 * Created by srinandchallur on 17/05/16.
 */

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class OrderPayment extends AbstractTimeStamp {
    private Long id;
    private Order order;
    private Transaction transaction;
    private String notes;
    private String status;
}
