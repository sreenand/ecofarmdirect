package in.nogyo.nogyoapp.econfarmdirect.checkout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.main.MainActivity;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.orders.OrderActivity;
import in.nogyo.nogyoapp.econfarmdirect.orders.OrdersHistoryActivity;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 23/05/16.
 */
@Getter @Setter @NoArgsConstructor
public class CheckoutTask extends AsyncTask<Void, Void, Order> {

    private CheckoutActivity activity;

    public  CheckoutTask (CheckoutActivity activity){
        this.activity = activity;
    }
    @Override
    protected Order doInBackground(Void... params) {

        Map<String,String> map = (Map<String,String>)activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll();
        try{
            User sessionUser = ((EcoFarmContext)activity.getApplicationContext()).getSessionUser();
            Store store = ((EcoFarmContext)activity.getApplicationContext()).getStore();
            RestUtil.mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
            CommonUtils.blockThisThread(3000L);
            return CheckoutUtil.completeCheckout(((EcoFarmContext)activity.getApplicationContext()).getCart(),store,sessionUser);
        } catch (Exception e){
            return null;
        }

    }

    @Override
    protected void onPostExecute(final Order order) {
        if(order == null){
            Toast.makeText(activity.getBaseContext(), "Unable to complete the transaction please try after some time", Toast.LENGTH_SHORT).show();
        } else {
            try {
                try {
                    Order confirmedOrder = order;
                    if(confirmedOrder == null)
                        throw new RuntimeException("Unable to place the order");

                    activity.getMCheckoutProgressView().setVisibility(View.GONE);
                    activity.getTotalCount().setVisibility(View.GONE);
                    ((TextView)activity.findViewById(R.id.order_id_value)).setText(order.getId().toString());
                    RelativeLayout layout = (RelativeLayout)activity.findViewById(R.id.order_confirmed);
                    layout.setVisibility(View.VISIBLE);

                    EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();
                    context.getCart().clear();
                    context.persistCart();
                    context.setSelectedOrder(order);

                } catch (Exception e) {
                    Log.e("ERROR", "Unable to place the order", e);
                    Toast.makeText(activity.getBaseContext(), "Unable to place the order", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(activity.getBaseContext(), "Your Order has been Placed Contact EcoFarm for further details", Toast.LENGTH_SHORT).show();
                Log.e("ERROR", "Error in serialzing Order");
                activity.startActivity(CommonUtils.navigate(activity, OrdersHistoryActivity.class));
            }
        }
    }
}
