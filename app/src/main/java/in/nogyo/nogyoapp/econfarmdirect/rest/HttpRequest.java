package in.nogyo.nogyoapp.econfarmdirect.rest;

import android.os.AsyncTask;

import org.codehaus.jackson.type.TypeReference;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 21/05/16.
 */

@Getter @Setter @AllArgsConstructor(suppressConstructorProperties = true) @NoArgsConstructor
public class HttpRequest extends AsyncTask<Void, Void, Response> {

    private String url;
    private String method;
    private Map<String,String> queryParameters;
    private Map<String,String> requestHeaders;
    private String requestBody;
    private int timeout;
    private String sessionId;
    private Response response;

    @Override
    protected Response doInBackground(Void... params) {
        return RestUtil.execute(url,requestBody,requestHeaders,queryParameters,method,timeout,sessionId);
    }
}
