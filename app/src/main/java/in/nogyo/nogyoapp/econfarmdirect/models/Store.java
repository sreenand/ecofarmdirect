package in.nogyo.nogyoapp.econfarmdirect.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 21/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class Store extends AbstractTimeStamp{

    private Long id;
    private Long userId;
    private Long salesUserId;
    private String name;
    private String contactPerson;
    private String type;
    private String phoneNo;
    private String address;
    private Long localityId;
    private Locality locality;
    private String status;
    private UserWallet userWallet;
}
