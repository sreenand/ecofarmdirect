package in.nogyo.nogyoapp.econfarmdirect.models;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */
@Getter @Setter
public class AbstractTimeStamp {

    protected Date createdAt;
    protected Date updatedAt;

    public AbstractTimeStamp()  {
        createdAt = new Date();
    }
}

