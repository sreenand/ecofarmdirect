package in.nogyo.nogyoapp.econfarmdirect.checkout;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.OrderItem;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;

/**
 * Created by srinandchallur on 23/05/16.
 */
public class CheckoutUtil {

    public static Order completeCheckout(Map<Long,ProductPricing> skuList, Store selectedStore, User sessionUser) throws IOException {

        Order order = new Order();
        order.setStatus("RECEIVED");
        order.setFulfillmentStatus("NOT_FULFILLED");
        order.setChannel(sessionUser.getType());
        order.setOrderDate(new Date());
        order.setUserId(sessionUser.getId());
        order.setStoreId(selectedStore.getId());
        List<OrderItem> orderItems = new ArrayList<>();

        Double amount = 0.0;
        Double discountedAmount;
        int bucketid;

        for(ProductPricing sku: skuList.values()){
            switch (isInBucket(sku)){
                case 1:
                    bucketid = 1;
                    discountedAmount = sku.getVariablePricing().get(0).discountedPrice;
                    break;
                case 2:
                    bucketid = 2;
                    discountedAmount = sku.getVariablePricing().get(1).discountedPrice;
                    break;
                case 3:
                    bucketid = 3;
                    discountedAmount = sku.getVariablePricing().get(2).discountedPrice;
                    break;
                default:
                    bucketid = 0;
                    discountedAmount = sku.getPricePerUnit();
            }
            orderItems.add(new OrderItem(null,null,selectedStore.getId(),sku.getId(),order.getStatus().toString(),order.getFulfillmentStatus().toString(),
                    sku.getProductUnit().getId(), sku.getProduct().getQuantity(),discountedAmount,bucketid));
            amount += sku.getProduct().getQuantity() * discountedAmount;
        }
        order.setOrderItems(orderItems);
        order.setPricePromised(Math.ceil(amount));
        Response response = RestUtil.execute(Constants.backend_orders_url,
                RestUtil.mapper.writeValueAsString(order), null,null, "POST",0, sessionUser.getSessionId());

        try{
            order = RestUtil.mapper.readValue(response.getResponseBody(),Order.class);
        }catch (Exception e){
            Log.e("ERROR", "Unable place the order Retry again");
            return null;
        }

        return order;
    }

    private static int isInBucket(ProductPricing productPricing){
        if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(0).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(0).max) {
            return 1;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(1).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(1).max){
            return 2;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(2).min){
            return 3;
        }
        return 0;
    }
}
