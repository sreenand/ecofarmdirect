package in.nogyo.nogyoapp.econfarmdirect.mydetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.orders.OrdersHistoryActivity;
import in.nogyo.nogyoapp.econfarmdirect.rest.HttpRequest;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;

public class MyDetailsActivity extends AppCompatActivity {

    private EcoFarmContext ecoFarmContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_details);

        ecoFarmContext = (EcoFarmContext)getApplicationContext();

        ((TextView) (findViewById(R.id.profile_name))).setText(ecoFarmContext.getSessionUser().getName());
        ((TextView) (findViewById(R.id.phone_no))).setText(ecoFarmContext.getSessionUser().getPhoneNo());
        ((TextView) (findViewById(R.id.address))).setText(ecoFarmContext.getSessionUser().getAddress());
        ((TextView) (findViewById(R.id.current_balance_value))).setText("");

        HttpRequest request = new HttpRequest(Constants.backend_tick_url,"GET",null,null,null,0,ecoFarmContext.getSessionUser().getSessionId(),null){

            @Override
            protected void onPostExecute(Response response){
                try {
                    User user = RestUtil.mapper.readValue(response.getResponseBody(),User.class);
                    String sessionId = ecoFarmContext.getSessionUser().getSessionId();
                    ecoFarmContext.setSessionUser(user);
                    ecoFarmContext.getSessionUser().setSessionId(sessionId);
                    ((TextView) (findViewById(R.id.current_balance_value))).setText(ecoFarmContext.getSessionUser().getUserWallet().getCurrentBalance().toString());
                } catch (Exception e) {
                    Log.e("ERROR","Exception in showing wallet balance in my details",e);
                }
            }
        };



        ((TextView) (findViewById(R.id.store_name))).setText(ecoFarmContext.getStore().getName());

        if(ecoFarmContext.getStore().getAddress() != null) {
            ((TextView) (findViewById(R.id.store_address))).setText(ecoFarmContext.getStore().getAddress() + "," +
                    ecoFarmContext.getStore().getLocality().getCity().getState());
        } else {
            ((TextView) (findViewById(R.id.store_address))).setText(ecoFarmContext.getStore().getLocality().getName() + "," +
                    ecoFarmContext.getStore().getLocality().getCity().getName() + "," +
                    ecoFarmContext.getStore().getLocality().getCity().getState());
        }




        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        request.execute((Void)null);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                break;
        }
        return true;
    }


    public void goToOrderHistory(View v){
        startActivity(CommonUtils.navigate(MyDetailsActivity.this, OrdersHistoryActivity.class));
    }
}
