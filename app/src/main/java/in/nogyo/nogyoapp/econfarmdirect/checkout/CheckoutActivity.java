package in.nogyo.nogyoapp.econfarmdirect.checkout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Product;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import in.nogyo.nogyoapp.econfarmdirect.orders.OrderActivity;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import in.nogyo.nogyoapp.econfarmdirect.skulist.SkuListActivity;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CheckoutActivity extends AppCompatActivity {

    private LinearLayout listView;
    private Map<Long,ProductPricing> products;
    private View mCheckoutProgressView;
    private Button checkOut;
    private Button clearCart;
    private Button cancelCheckout;
    private String language;
    private TextView totalCount;
    private EcoFarmContext context;
    private CheckoutTask checkoutTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try{

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_checkout);
            listView = (LinearLayout)findViewById(R.id.checkout_list);
            mCheckoutProgressView = findViewById(R.id.checkout_progress);
            checkOut = (Button)findViewById(R.id.checkout_button);
            clearCart = (Button)findViewById(R.id.clear_cart_button);
            context = (EcoFarmContext)getApplicationContext();
            language = context.getSelectedLanguage();
            totalCount = (TextView)(findViewById(R.id.totalCount));
            products = context.getCart();
            for(ProductPricing product : products.values()){
                product.getProduct().setLocalNamesMap(RestUtil.mapper.readValue(product.getProduct().getLocalNames(), HashMap.class));
            }
            totalCount.setText("No. Of Items:" + products.size());
        }catch(Exception e){
            products = null;
        }

        if(products == null || products.isEmpty()){
            Toast.makeText(getBaseContext(), "Please select Something", Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }

        View iview = getLayoutInflater().inflate(R.layout.checkout_row, null);
        ((TextView)iview.findViewById(R.id.checkout_grocery_name)).setText("Product");
        ((TextView)iview.findViewById(R.id.checkout_grocery_quantity)).setText("Qty");
        ((TextView)iview.findViewById(R.id.checkout_grocery_ppu)).setText("Price/Unit");
        ((TextView)iview.findViewById(R.id.checkout_grocery_item_total)).setText("Item Total");
        listView.addView(iview);

        Double totalAmount = 0.0;
        Double discountedAmount;

        for(ProductPricing sku: products.values()){
            View view = getLayoutInflater().inflate(R.layout.checkout_row, null);
            TextView view1 = (TextView)view.findViewById(R.id.checkout_grocery_name);
            TextView view2 = (TextView)view.findViewById(R.id.checkout_grocery_quantity);
            view1.setText(sku.getProduct().getLocalName(language));
            view2.setText(sku.getProduct().getQuantity() + " " + sku.getProductUnit().getUnitMetric());

            switch (isInBucket(sku)){
                case 1:
                    discountedAmount = sku.getVariablePricing().get(0).discountedPrice;
                    break;
                case 2:
                    discountedAmount = sku.getVariablePricing().get(1).discountedPrice;
                    break;
                case 3:
                    discountedAmount = sku.getVariablePricing().get(2).discountedPrice;
                    break;
                default:
                    discountedAmount = sku.getPricePerUnit();
            }
            ((TextView)view.findViewById(R.id.checkout_grocery_ppu)).setText("\u20B9" + discountedAmount + "/" + sku.getProductUnit().getUnitMetric());
            ((TextView)view.findViewById(R.id.checkout_grocery_item_total)).setText("\u20B9"  + (discountedAmount * sku.getProduct().getQuantity()));
            listView.addView(view);
            totalAmount += (discountedAmount*sku.getProduct().getQuantity());
        }

        if(totalAmount > 0){
            View gview = getLayoutInflater().inflate(R.layout.checkout_total,null);
            ((TextView)gview.findViewById(R.id.grand_total)).setText("₹" + totalAmount.toString());
            listView.addView(gview);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_checkout, menu);
//        return true;
//    }

    @Override
    public void onBackPressed(){
        startActivity(CommonUtils.navigate(CheckoutActivity.this, SkuListActivity.class));
        this.finish();
    }

    private int isInBucket(ProductPricing productPricing){
        if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(0).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(0).max) {
            return 1;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(1).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(1).max){
            return 2;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(2).min){
            return 3;
        }
        return 0;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                break;
        }

        return true;
    }

    public void confirmOrder(View view) {

        Calendar cal = Calendar.getInstance();
        boolean checkout = true;
        try {

            if(context.getSessionUser().getUserWallet().getCurrentBalance() < context.getSessionUser().getUserWallet().getLowerLimit()){

                checkout = false;
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setTitle("Insufficient Funds");
                builder1.setMessage("You do not have enough balance to make this transaction. Please pay the pending amount immediately");
                builder1.setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                CheckoutActivity.this.finish();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }

            if(!checkout) return;
            checkoutTask = new CheckoutTask(this);
            checkOut.setVisibility(View.GONE);
            clearCart.setVisibility(View.GONE);
            totalCount.setVisibility(View.GONE);
            mCheckoutProgressView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            Toast.makeText(getBaseContext(), "Please Wait While we Confirm Your Order", Toast.LENGTH_LONG).show();
            if(checkoutTask.getStatus().equals(AsyncTask.Status.PENDING)) {
                checkoutTask.execute();
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Unable to place order Try after some time", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        }

    }

    public void cancelOrder(View view){
        if(checkoutTask.getStatus().equals(AsyncTask.Status.PENDING)){
            checkoutTask.cancel(true);
            mCheckoutProgressView.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            checkOut.setVisibility(View.VISIBLE);
            clearCart.setVisibility(View.VISIBLE);
            Toast.makeText(getBaseContext(), "Order Cancelled", Toast.LENGTH_SHORT).show();
        }
    }


    public void clearCart(View view) {

        Calendar cal = Calendar.getInstance();
        try {
             context.getCart().clear();
             context.persistCart();
             listView.setVisibility(View.GONE);
             totalCount.setVisibility(View.GONE);
             mCheckoutProgressView.setVisibility(View.VISIBLE);
             CommonUtils.blockThisThread(100L);
             Toast.makeText(getBaseContext(), "Your Cart is Clear", Toast.LENGTH_LONG).show();
             Intent intent = CommonUtils.navigate(CheckoutActivity.this, SkuListActivity.class);
             intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             this.startActivity(intent);
             this.finish();
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "Unable to place order Try after some time", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        }


    }


    public void confirmCheckout(final View view){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        confirmOrder(view);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Confirm Order");
        builder.setMessage("Do you wish to confirm this order ?").setPositiveButton("YES", dialogClickListener)
                .setNegativeButton("NO", dialogClickListener).show();

    }

    public void viewOrderDetails(View view){
        Intent intent = CommonUtils.navigate(this, OrderActivity.class);
        intent.putExtra(Constants.CALLER_ACTIVITY, String.valueOf(Constants.ACTIVITY_CHECKOUT));
        startActivity(intent);
        this.finish();
    }
}
