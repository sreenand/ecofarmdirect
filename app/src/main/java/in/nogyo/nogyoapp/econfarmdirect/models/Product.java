package in.nogyo.nogyoapp.econfarmdirect.models;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class Product extends AbstractTimeStamp {

    private Long id;
    private String name;
    private String description;
    private int quantity;
    private boolean selected;
    private String imageUrl;
    private Long subCategoryId;
    private SubCategory subCategory;;
    private String localNames;

    @JsonIgnore
    private Map<String,String> localNamesMap;

    @JsonIgnore
    public String getLocalName(String language){

        if(localNamesMap == null || localNamesMap.isEmpty() || language == null || language.isEmpty()
                || language.toLowerCase().equals("english"))
            return name;

        return localNamesMap.get(language);
    }

}
