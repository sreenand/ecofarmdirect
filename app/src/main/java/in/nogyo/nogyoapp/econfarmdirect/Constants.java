package in.nogyo.nogyoapp.econfarmdirect;


/**
 * Created by srinand.pc on 21/02/16.
 */
public class Constants {

    public static final String backend_base_url = URLs.backend_base_url;
    public static final String backend_login_url = backend_base_url + "/users/login";
    public static final String backend_tick_url = backend_base_url + "/users/tick";
    public static final String backend_version_url = backend_base_url + "/version";
    public static final String backend_user_url = backend_base_url + "/users";
    public static final String backend_stores_url = backend_base_url + "/stores";
    public static final String backend_mystores_url = backend_stores_url + "/mystores";
    public static final String backend_all_stores_url = backend_base_url + "/stores/all";
    public static final String backend_demand_add_url = backend_base_url + "/demand/add/bulk";
    public static final String backend_orders_url = backend_base_url + "/orders";
    public static final String backend_sku_url = backend_base_url + "/sku";

    public static final String ORDER_ID = "Order ID:";
    public static final String ORDER_STATUS = "Order Status:";

    public static final String SELECTED_STORE = "SELECTED_STORE";
    public static final String SESSION_CART = "SELECTED_CART";
    public static final String SESSION_USER = "SESSION_USER";
    public static final String SESSION_DATA = "SESSION_DATA";
    public static final String CART = "CART";
    public static final String SESSION_ID = "SESSION_ID";
    public static final String ORDER_SELECTED = "ORDER_SELECTED";
    public static final String LANGUAGE_SELECTED = "LANGUAGE_SELECTED";
    public static final String CALLER_ACTIVITY = "CALLER_ACTIVITY";
    public static final String ACTIVITY_CHECKOUT = "CHECKOUT";
    public static final String ACTIVITY_ORDER_HISTORY = "ORDER_HISTORY";
    public static final String K1 = "K1";
    public static final String K2 = "K2";
    public static final String K3 = "K3";
    public static final int CURRENT_VERSION = 2;
}
