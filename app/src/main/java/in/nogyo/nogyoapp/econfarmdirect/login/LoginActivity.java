package in.nogyo.nogyoapp.econfarmdirect.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.rest.HttpRequest;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import in.nogyo.nogyoapp.econfarmdirect.skulist.SkuListActivity;

public class LoginActivity extends AppCompatActivity {

    private static final int REQUEST_READ_CONTACTS = 0;

    private AutoCompleteTextView mPhoneNoView;
    private EditText mPasswordView;
    private String language;
    private View mProgressView;
    private View mLoginFormView;
    private EcoFarmContext context;
    private long mBackPressed;
    private static final int TIME_INTERVAL = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            getSupportActionBar().hide();
            setContentView(R.layout.activity_login);

            context = (EcoFarmContext)getApplicationContext();
            mPhoneNoView = (AutoCompleteTextView)this.findViewById(R.id.mobile);
            mProgressView = this.findViewById(R.id.titleImageProgressBar);
            mPasswordView  = (EditText)this.findViewById(R.id.password);
            mLoginFormView = this.findViewById(R.id.login_form);
            language = CommonUtils.getLanguage(((Spinner) findViewById(R.id.language)).getSelectedItemPosition());


            Map<String,String> data = (Map<String,String>)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll();

            String k1 = data.get(Constants.K1);
            String k2 = data.get(Constants.K2);
            mPhoneNoView.setText(k1);
            mPasswordView.setText(k2);
        }catch (Exception e){
            Log.e("ERROR","Error in Login Activity",e);
        }
    }


    public void login(View v){
        language = CommonUtils.getLanguage(((Spinner) findViewById(R.id.language)).getSelectedItemPosition());
        mLoginFormView.setVisibility(View.INVISIBLE);
        doLogin(mPhoneNoView.getText().toString(), mPasswordView.getText().toString(), language);
    }

    private void doLogin(final String mobile , final String password, final String language){


        try {
            this.mProgressView.setVisibility(View.VISIBLE);
            this.mLoginFormView.setVisibility(View.GONE);
            Map<String,String> userLoginRequest = new HashMap<>();
            userLoginRequest.put("username",mobile);
            userLoginRequest.put("password",password);
            HttpRequest loginRequest = new HttpRequest(Constants.backend_login_url,"POST",null,null,
                    RestUtil.mapper.writeValueAsString(userLoginRequest),1000,null,null){

                protected void onPostExecute(Response result){

                    if(result.getResponseCode() == 200){
                        try {
                            User user = RestUtil.mapper.readValue(result.getResponseBody(), User.class);
                            if(!user.isStore()){
                                throw new RuntimeException("Not a store user");
                            }
                            user.setSessionId(result.getCookies().get("JSESSIONID"));
                            context.setSelectedLanguage(language);
                            context.setSessionUser(user);
                            SharedPreferences sharedpreferences = getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Constants.K1, mobile);
                            editor.putString(Constants.K2, password);
                            editor.putString(Constants.K3, context.getSessionUser().getSessionId());
                            editor.commit();

                            HttpRequest request = new HttpRequest(Constants.backend_mystores_url,"GET",null,null,null,100,context.getSessionUser().getSessionId(),null){
                                protected void onPostExecute(Response result){
                                    try {
                                        List<Store> storeList = RestUtil.mapper.readValue(result.getResponseBody(), new TypeReference<List<Store>>() {});
                                        mProgressView.setVisibility(View.GONE);
                                        context.setStore(storeList.get(0));
                                        context.setStoreCity(storeList.get(0).getLocality().getCity());
                                        startActivity(CommonUtils.navigate(LoginActivity.this, SkuListActivity.class));
                                    } catch (IOException e) {
                                        Log.e("ERROR","Unable to set the store",e);
                                        return;
                                    }
                                }
                            };
                            request.execute((Void)null);
                        } catch (Exception e) {
                            Log.e("ERROR", "Login Failed", e);
                            Toast.makeText(getBaseContext(), "Login Failed !!!."+e.getMessage(), Toast.LENGTH_SHORT).show();
                            mLoginFormView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mLoginFormView.setVisibility(View.VISIBLE);
                        Toast.makeText(getBaseContext(), "Login Failed !!!.", Toast.LENGTH_SHORT).show();
                    }
                }
            };

            loginRequest.execute((Void)null);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Login Failed !!!."+e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("ERROR","Login Failed",e);
        }

    }


    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        } else {
            Toast.makeText(getBaseContext(), "Tap back button in order to exit", Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }
}
