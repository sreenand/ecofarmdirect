package in.nogyo.nogyoapp.econfarmdirect.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 12/05/16.
 */

@Getter @Setter @NoArgsConstructor
public class UserWallet extends AbstractTimeStamp {
    private Long id;

    private Long userId;

    private Double currentBalance;

    private Double upperLimit;

    private Double lowerLimit;
}
