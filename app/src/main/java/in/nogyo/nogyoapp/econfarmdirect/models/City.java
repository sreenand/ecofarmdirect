package in.nogyo.nogyoapp.econfarmdirect.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 27/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class City extends AbstractTimeStamp {

    private int id;
    private String name;
    private String state;
    private Boolean serviceable;
    private String extraAttributes;

}
