package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.OrderItem;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;


/**
 * Created by srinandchallur on 06/04/16.
 */
public class OrderUtil {


    public static List<Order> getOrdersForStore(Long storeId,int limit, int offset,Activity activity){
        List<Order> orderList = new ArrayList<>();

        EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();
        String sessionId = context.getSessionUser().getSessionId();

        StringBuffer url = new StringBuffer();
        url = url.append(Constants.backend_stores_url).append("/").append(storeId.toString()).append("/orders");
        url = url.append(";jsessionid=").append(sessionId).append("?limit=").append(limit).append("&offset=").append(offset);

        try{
            Response response = RestUtil.execute(url.toString(),
                    null, null,null, "GET", 100, null);

            orderList = RestUtil.mapper.readValue(response.getResponseBody(), new TypeReference<List<Order>>() {});
        } catch (Exception e){
            Log.e("ERROR","Unable to get Orders",e);
        }
        return orderList;
    }


    public static List<OrderItem> getOrderItemsForOrder(Long orderId,int limit, int offset,Activity activity){
        List<OrderItem> orderItemList = new ArrayList<>();

        EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();
        String sessionId = context.getSessionUser().getSessionId();

        StringBuffer url = new StringBuffer();
        url = url.append(Constants.backend_orders_url).append("/").append(orderId.toString()).append("/orderitems");
        url = url.append(";jsessionid=").append(sessionId).append("?limit=").append(limit).append("&offset=").append(offset);

        try{
            Response response = RestUtil.execute(url.toString(),
                    null, null,null, "GET", 100, null);

            orderItemList = RestUtil.mapper.readValue(response.getResponseBody(), new TypeReference<List<OrderItem>>() {});
        } catch (Exception e){
            Log.e("ERROR","Unable to get Orders",e);
        }
        return orderItemList;
    }
}
