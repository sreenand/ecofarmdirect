package in.nogyo.nogyoapp.econfarmdirect.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 22/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class Response {

    private int responseCode;
    private String responseBody;
    private Map<String, String> headers;
    private Map<String, String> cookies;


    public void setHeaders(Map<String,List<String>> headers){


        Map<String,String> responseCookies = null;
        Map<String,String> responseHeaders = null;
        List<String> cookies = headers.get("Set-Cookie");

        if(headers != null){
            responseHeaders = new HashMap<String,String>();
            for(Map.Entry<String,List<String>> entry : headers.entrySet()){

                if(entry.getKey() != null && entry.getKey().equals("Set-Cookie"))
                    continue;
                responseHeaders.put(entry.getKey(), entry.getValue().get(0));
            }
        }

        if(cookies != null){
            String cookieString = cookies.get(0);
            String[] cookieList = cookieString.split(";");
            responseCookies = new HashMap<String,String>();
            for(String cookie : cookieList){
                String[] keyValuePair = cookie.split("=");
                responseCookies.put(keyValuePair[0],keyValuePair[1]);
            }
        }

        if(responseCookies != null){
            setCookies(responseCookies);
        }
    }
}
