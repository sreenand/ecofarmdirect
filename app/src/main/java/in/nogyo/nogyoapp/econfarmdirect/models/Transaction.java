package in.nogyo.nogyoapp.econfarmdirect.models;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 17/05/16.
 */

@Getter @Setter @NoArgsConstructor
public class Transaction extends AbstractTimeStamp  {

    private Long id;
    private List<TransactionItem> transactionItems;
    private String externalId;
    private Long donor;
    private Long recepient;
    private Double amount;
    private Double amountReconciled;
    private String reconcilationStatus;
    private String currency;
    private String type;
    private String paymentMode;
    private Date paymentDate;
}
