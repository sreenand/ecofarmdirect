package in.nogyo.nogyoapp.econfarmdirect.skulist;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import org.codehaus.jackson.type.TypeReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import in.nogyo.nogyoapp.econfarmdirect.models.SubCategory;
import in.nogyo.nogyoapp.econfarmdirect.models.VariablePricing;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;

/**
 * Created by srinandchallur on 22/05/16.
 */
public class SKUUtil {

    public static List<ProductPricing> getAllSKUPricingForSubCategory(SubCategory subCategory, Activity activity){

        EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();

        List<ProductPricing> productPricingList;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        HashMap<String,String> queryParams = new HashMap<>();
        queryParams.put("city_id",""+context.getStoreCity().getId());
        queryParams.put("enabled","true");
        queryParams.put("sub_cat_id",subCategory.getId().toString());

        Response response = RestUtil.execute(Constants.backend_sku_url+"/query",
                null, null, queryParams, "GET", 100, context.getSessionUser().getSessionId());

        try {
            Log.i("response", response.getResponseBody());
            productPricingList = RestUtil.mapper.readValue(response.getResponseBody(), new TypeReference<List<ProductPricing>>(){});

            for(ProductPricing productPricing : productPricingList){
                if(productPricing.getProduct().getLocalNames() != null){
                    productPricing.getProduct().setLocalNamesMap(RestUtil.mapper.readValue(productPricing.getProduct().getLocalNames(), HashMap.class));
                }
            }
        } catch (Exception e) {
            productPricingList = new ArrayList<>();
            Log.e("ERROR", "Error in getting store list from server", e);
        }
        return productPricingList;
    }


    public static List<VariablePricing> getAllSKUVariablePricingForSubCategory(Long productId, Activity activity){

        EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();

        List<VariablePricing> variablePricingList = null;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        HashMap<String,String> queryParams = new HashMap<>();
        queryParams.put("product_id",""+productId);

        Response response = RestUtil.execute(Constants.backend_sku_url+"/variableprice",
                null, null, queryParams, "GET", 100, context.getSessionUser().getSessionId());

        try {
            //variablePricingList = RestUtil.mapper.readValue(response.getResponseBody(), new TypeReference<List<VariablePricing>>(){});
        } catch (Exception e) {
            variablePricingList = new ArrayList<>();
            Log.e("ERROR", "Error in getting store list from server", e);
        }
        return variablePricingList;
    }


}
