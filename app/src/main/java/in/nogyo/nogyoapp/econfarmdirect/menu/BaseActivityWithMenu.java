package in.nogyo.nogyoapp.econfarmdirect.menu;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.checkout.CheckoutActivity;
import in.nogyo.nogyoapp.econfarmdirect.mydetails.MyDetailsActivity;
import in.nogyo.nogyoapp.econfarmdirect.orders.OrdersHistoryActivity;

/**
 * Created by srinand.pc on 04/03/16.
 */

public abstract class BaseActivityWithMenu extends AppCompatActivity {


    protected enum SliderMenuItem {MY_DETAILS("My Details"),ORDER_HISTORY("My Orders"),MY_CART("My Cart"),LOGOUT("Log Out");

        private String value;

        SliderMenuItem(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return this.getValue();
        }

        public static SliderMenuItem getEnum(String value) {
            for(SliderMenuItem v : values())
                if(v.getValue().equalsIgnoreCase(value)) return v;
            throw new IllegalArgumentException();
        }
    };
    protected DrawerLayout mDrawerLayout;
    protected LinearLayout mDrawerList;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected CharSequence mDrawerTitle;
    protected CharSequence mTitle;
    protected String[] navMenuTitles;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private Method method;




    protected void populateMenu() {

        for (NavDrawerItem item : navDrawerItems) {
            LayoutInflater mInflater = (LayoutInflater) getLayoutInflater();
            View inflatedView = mInflater.inflate(R.layout.menu_list_item, null);
            TextView textView = (TextView) inflatedView.findViewById(R.id.title);
            textView.setText(item.getTitle());
            mDrawerList.addView(inflatedView);
        }
    }


    protected void createMenu() {

        mDrawerList = (LinearLayout) findViewById(R.id.list_slidermenu);
        navMenuTitles = getResources().getStringArray(R.array.menu_items);
        TypedArray menuImages = getResources().obtainTypedArray(R.array.menu_icons);
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        for(int i=0;i<navMenuTitles.length;i++){
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], menuImages.getResourceId(i,-1)));
        }

        mDrawerList.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        NavDrawerListAdapter adapter = new NavDrawerListAdapter(this, navDrawerItems);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.mipmap.ic_menu_button, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        LayoutInflater mInflater = (LayoutInflater)
                this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        EcoFarmContext context = (EcoFarmContext)this.getApplicationContext();

        ((TextView)(findViewById(R.id.welcome_text))).setText("Welcome " + context.getSessionUser().getName());

        for(NavDrawerItem item: navDrawerItems){

            mInflater = (LayoutInflater)
                    this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            View convertView = mInflater.inflate(R.layout.menu_list_item,null);
            final TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
            final ImageView imageView = (ImageView) convertView.findViewById(R.id.menu_image);
            Picasso.with(this).load(item.getIcon()).into(imageView);
            txtTitle.setText(item.getTitle());
            txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayView(txtTitle);
                }
            });
            mDrawerList.addView(convertView);
        }


        //populateMenu();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void displayView(View view) {
        TextView view1 = (TextView) view;
        displayView(SliderMenuItem.getEnum(view1.getText().toString()));
    }

    private void displayView(SliderMenuItem menu) {

        mDrawerLayout.closeDrawer(mDrawerList);
        switch (menu) {
            case MY_DETAILS:
                CommonUtils.navigateTo(this, MyDetailsActivity.class);
                break;

            case MY_CART:
                CommonUtils.navigateTo(this, CheckoutActivity.class);
                break;

            case ORDER_HISTORY:
                CommonUtils.navigateTo(this, OrdersHistoryActivity.class);
                break;
            case LOGOUT:
                logOut();
                break;
            default:
                break;
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public abstract void logOut();


}
