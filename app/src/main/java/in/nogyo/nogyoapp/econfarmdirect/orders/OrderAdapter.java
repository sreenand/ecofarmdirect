package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;


/**
 * Created by srinand.pc on 26/02/16.
 */
public class OrderAdapter extends BaseAdapter {

    private OrderActivity activity;
    private Order order;

    public OrderAdapter(OrderActivity activity, Order order){
        this.activity = activity;
        this.order = order;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = null;
        if(convertView == null){
            view = new TextView(activity);
            view.setTextSize(TypedValue.COMPLEX_UNIT_DIP,25);
            switch (position) {
                case 0:
                    view.setText(R.string.order_id);
                    break;
                case 1:
                    view.setText(order.getId().toString());
                    break;
                case 2:
                    view.setText(R.string.order_status);
                    break;
                case 3:
                    view.setText(order.getStatus());
                    break;
            }
        } else{
            view = (TextView)convertView;
        }
        return view;
    }
}
