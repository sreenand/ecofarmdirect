package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.os.AsyncTask;
import android.view.View;

import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 06/04/16.
 */

@Getter @Setter
public class OrderHistoryPopulateTask extends AsyncTask<Void, Void, Boolean> {


    private List<Order> orderList;
    private Long storeId;
    private int offset;
    private int limit;
    private OrdersHistoryActivity activity;

    public OrderHistoryPopulateTask (Long storeId, int limit, int offset, OrdersHistoryActivity activity){
        this.storeId = storeId;
        this.limit = limit;
        this.offset = offset;
        this.activity = activity;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        activity.setLoadingMore(true);
        orderList = OrderUtil.getOrdersForStore(storeId,limit,offset,activity);
//        if(orderList.size() == 0 && activity.getOrderList().size() == 0)
//            activity.getNoOrdersText().setVisibility(View.VISIBLE);
        activity.getOrderList().addAll(orderList);
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        activity.getProgressBar().setVisibility(View.GONE);
        activity.getOrderHistoryAdapter().notifyDataSetChanged();
        activity.setLoadingMore(false);
        if(getOrderList().size() < 10){
            activity.setFetchData(false);
        }
    }
}
