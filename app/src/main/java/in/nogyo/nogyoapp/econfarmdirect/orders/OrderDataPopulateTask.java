package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 25/02/16.
 */

@Getter @Setter
public class OrderDataPopulateTask extends AsyncTask<Void, Void, Store>{

    private OrderActivity activity;
    private Store store;


    public OrderDataPopulateTask(OrderActivity activity, Store store){
        this.activity = activity;
        this.store = store;
    }


    @Override
    protected Store doInBackground(Void... params) {

        try {
            EcoFarmContext context = (EcoFarmContext)activity.getApplicationContext();
            return context.getStore();
        } catch (Exception e) {
           return null;
        }

    }

    @Override
    public void onPostExecute(Store store){
        this.setStore(store);
        //populateStoreData();
    }

    private void populateStoreData(){

        if(store == null)
            return;

        View storeView = activity.getLayoutInflater().inflate(R.layout.order_view_store_content, null);
        ((TextView)(storeView.findViewById(R.id.store_name_value))).setText(store.getName());
        ((TextView)(storeView.findViewById(R.id.store_owner_value))).setText(store.getContactPerson());
        ((TextView)(storeView.findViewById(R.id.store_address_value))).setText(store.getAddress());
        activity.getLayout().addView(storeView);
    }
}
