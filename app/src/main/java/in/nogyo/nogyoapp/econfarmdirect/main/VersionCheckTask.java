package in.nogyo.nogyoapp.econfarmdirect.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.rest.Response;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.java.Log;

/**
 * Created by srinandchallur on 26/05/16.
 */
@Getter @Setter @AllArgsConstructor(suppressConstructorProperties = true) @NoArgsConstructor
public class VersionCheckTask extends AsyncTask<Void, Void, Response> {

    private Activity activity;

    @Override
    protected Response doInBackground(Void... params) {
        return RestUtil.execute(Constants.backend_version_url,null,null,null,"GET",0,null);
    }

    protected void onpostExecute(Response response){

        boolean launchPlayStore = false;
        if(response.getResponseCode() == 200){

            try {
                HashMap<String,Object> result = RestUtil.mapper.readValue(response.getResponseBody(), HashMap.class);

                if(!((List<Integer>)result.get("supportedVersions")).contains(Constants.CURRENT_VERSION)){
                    launchPlayStore = true;
                }
            } catch (IOException e) {
                android.util.Log.e("ERROR","Unable deserialize version info",e);
            }

            if(launchPlayStore){
                ((EcoFarmContext)activity.getApplicationContext()).setLaunchPlayStore(true);
                final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                try {
                                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                }
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                Toast.makeText(activity, "Update is mandatory.Quitting the app.", Toast.LENGTH_SHORT).show();
                                CommonUtils.blockThisThread(100L);
                                System.exit(1);
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                builder.setTitle("Update required");
                builder.setMessage("Your app needs to be updated. Would you like to update it now ?").setPositiveButton("Update", dialogClickListener)
                        .setNegativeButton("Later", dialogClickListener).show();
            } else {
                ((EcoFarmContext)activity.getApplicationContext()).setLaunchPlayStore(false);
            }

        }
    }
}
