package in.nogyo.nogyoapp.econfarmdirect.menu;

/**
 * Created by srinand.pc on 04/03/16.
 */
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.nogyo.nogyoapp.econfarmdirect.R;


public class MenuFragment extends Fragment {

    public MenuFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.menu_fragment, container, false);
        return rootView;
    }
}
