package in.nogyo.nogyoapp.econfarmdirect.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 27/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class ProductUnit extends AbstractTimeStamp {

    private Long id;
    private String unitName;
    private Double unitQuantity;
    private String unitType;
    private String unitMetric;
    private String unitDescription;
}
