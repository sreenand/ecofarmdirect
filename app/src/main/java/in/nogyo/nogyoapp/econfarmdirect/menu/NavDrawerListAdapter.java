package in.nogyo.nogyoapp.econfarmdirect.menu;

/**
 * Created by srinand.pc on 04/03/16.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.nogyo.nogyoapp.econfarmdirect.R;


public class NavDrawerListAdapter extends BaseAdapter {

    private BaseActivityWithMenu activityWithMenu;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public NavDrawerListAdapter(BaseActivityWithMenu activityWithMenu, ArrayList<NavDrawerItem> navDrawerItems){
        this.activityWithMenu = activityWithMenu;
        this.navDrawerItems = navDrawerItems;
    }


    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    activityWithMenu.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.menu_list_item, null);
        }
        final TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityWithMenu.displayView(txtTitle);
            }
        });

        ImageView imageView = (ImageView)(convertView.findViewById(R.id.menu_image));
        Picasso.with(activityWithMenu).load(navDrawerItems.get(position).getIcon()).into(imageView);
        return convertView;
    }

}

