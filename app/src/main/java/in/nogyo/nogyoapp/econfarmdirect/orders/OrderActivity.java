package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.CommonUtils;
import in.nogyo.nogyoapp.econfarmdirect.Constants;
import in.nogyo.nogyoapp.econfarmdirect.EcoFarmContext;
import in.nogyo.nogyoapp.econfarmdirect.R;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.OrderItem;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import in.nogyo.nogyoapp.econfarmdirect.skulist.SkuListActivity;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class OrderActivity extends AppCompatActivity {


    private Order order;
    private List<OrderItem> orderItemList;
    private GridView orderGridView;
    private LinearLayout layout;
    private OrderAdapter adapter;
    private Store selectedStore;
    private User sessionUser;
    private boolean fetchData = true;
    private boolean loadingMore = false;
    private ListView listView;
    private OrderItemListAdapter orderItemListAdapter;
    private OrderItemOnScrollListener orderItemOnScrollListener;
    private String language;
    private String caller;
    private EcoFarmContext context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_order);
            orderItemList = new ArrayList<>();
            Bundle extras = getIntent().getExtras();
            if(extras != null)
                caller = (String)extras.get(Constants.CALLER_ACTIVITY);
            context = (EcoFarmContext)getApplicationContext();

            this.setTitle(this.getTitle() +" " +context.getSelectedOrder().getId().toString());
            listView = (ListView)findViewById(R.id.order_item_list);
            layout = (LinearLayout)findViewById(R.id.order_data_layout);
            OrderDataPopulateTask task = new OrderDataPopulateTask(this,null);
            populateOrder();
            populateOrderData();
            task.execute((Void) null);

            this.orderItemListAdapter = new OrderItemListAdapter(orderItemList,this);
            orderItemOnScrollListener = new OrderItemOnScrollListener(this);
            this.listView.setOnScrollListener(this.orderItemOnScrollListener);
            this.listView.setAdapter(this.orderItemListAdapter);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }catch (Exception e){
            Log.e("ERROR", "Unable to load Order View", e);
        }
    }


    public void populateOrder() {
        this.order = context.getSelectedOrder();
    }


    private void populateOrderData(){

        View view1  = getLayoutInflater().inflate(R.layout.order_row,null);
        TextView viewHolder = (TextView)view1.findViewById(R.id.order_id_value);
        viewHolder.setText(this.order.getId().toString());
        viewHolder = (TextView)view1.findViewById(R.id.order_status_value);
        viewHolder.setText(this.order.getStatus().toString());
        ((TextView)(view1.findViewById(R.id.order_price_value))).setText(this.order.getPriceCalculated() != null ?
                this.order.getPriceCalculated().toString() : this.order.getPricePromised().toString());
        ((TextView)(view1.findViewById(R.id.payment_status_value))).setText(this.order.getOrderPayment().getStatus());
        this.layout.addView(view1);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                break;
        }

        return true;
    }


    public void orderAgain(View view) {
        startActivity(CommonUtils.navigate(OrderActivity.this, SkuListActivity.class));
    }

    @Override
    public void onBackPressed(){
        this.finish();
        if(Constants.ACTIVITY_ORDER_HISTORY.equals(caller)) {
            super.onBackPressed();
            return;
        }
        startActivity(CommonUtils.navigate(OrderActivity.this, SkuListActivity.class));
        return;
    }
}
