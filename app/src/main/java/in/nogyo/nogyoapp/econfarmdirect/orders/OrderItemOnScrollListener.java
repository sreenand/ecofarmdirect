package in.nogyo.nogyoapp.econfarmdirect.orders;

import android.util.Log;
import android.widget.AbsListView;

import lombok.Getter;
import lombok.Setter;


/**
 * Created by srinandchallur on 08/04/16.
 */

@Getter @Setter
public class OrderItemOnScrollListener implements AbsListView.OnScrollListener {

    private OrderActivity orderActivity;


    public OrderItemOnScrollListener(OrderActivity orderActivity){
        this.orderActivity = orderActivity;
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int lastInScreen = firstVisibleItem + visibleItemCount;
        if((lastInScreen == totalItemCount) && !(orderActivity.isLoadingMore()) && orderActivity.isFetchData()){
            try{
                OrderItemPopulateTask task =
                        new OrderItemPopulateTask(10,orderActivity.getOrderItemList().size(),orderActivity,null);
                task.execute((Void) null);

            } catch (Exception e){
                Log.e("ERROR", "Unable to get orderItem Data", e);
            }
        }

    }
}
