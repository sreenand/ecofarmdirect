package in.nogyo.nogyoapp.econfarmdirect.models;

/**
 * Created by srinandchallur on 17/05/16.
 */
public class TransactionItem extends AbstractTimeStamp {

    private Long id;
    private Transaction transaction;
    private String externalId;
    private Double amount;
    private String reconcilationStatus;
    private String reconcilationTxnItemId;
}
