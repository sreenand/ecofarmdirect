package in.nogyo.nogyoapp.econfarmdirect.rest;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

/**
 * Created by srinand.pc on 21/02/16.
 */
public class RestUtil {


    public static final ObjectMapper mapper = new ObjectMapper();
    public static final Logger logger = LoggerFactory.getLogger(RestUtil.class);


    public static Response execute(String url, String body, Map<String, String> headers, Map<String, String> queryParams,
                                   String method, int timeout, String sessionId) {


        Response response = null;
        StringBuilder sessionUrl = new StringBuilder(url);
        if (sessionId != null) {
            sessionUrl.append(";jsessionid=").append(sessionId);
        }

        if (queryParams != null && queryParams.size() > 0) {
            sessionUrl.append("?");
        }

        if (queryParams != null) {
            for (Map.Entry<String, String> entry : queryParams.entrySet()) {

                if (entry != null && entry.getValue() != null & entry.getKey() != null) {
                    sessionUrl.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
                }
            }
            sessionUrl.deleteCharAt(sessionUrl.length() - 1);
        }


        URL obj = null;
        StringBuilder sb = new StringBuilder();
        HttpURLConnection con = null;
        try {
            response = new Response();
            obj = new URL(sessionUrl.toString());
            con = (HttpURLConnection) obj.openConnection();
            HttpConnectionBuilder.valueOf(method.toUpperCase()).setRequestMethod(con);

            if (body != null) {
                con.setDoOutput(true);
                if (timeout > 0) {
                    con.setReadTimeout(timeout);
                }
                con.setRequestProperty("Content-Type", "application/json");
                if (headers != null) {
                    for (Map.Entry<String, String> entry : headers.entrySet())
                        con.setRequestProperty(entry.getKey(), entry.getValue());
                }
                OutputStream os = con.getOutputStream();
                os.write(body.getBytes("UTF-8"));
                os.flush();
            }

            response.setResponseCode(con.getResponseCode());
            BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
            response.setResponseBody(sb.toString());
            response.setHeaders(con.getHeaderFields());
        } catch (Exception e) {
            logger.error("Unable to execute the URL:", e);
        } finally {
            con.disconnect();
        }
        return response;
    }
}
