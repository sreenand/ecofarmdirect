package in.nogyo.nogyoapp.econfarmdirect.models;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 13/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class Category {

    private Long id;
    private String name;
    private String localNames;
    private Map<String,String> localNamesMap;


    @JsonIgnore
    public String getLocalName(String language){

        if(localNamesMap == null || localNamesMap.isEmpty() || language == null || language.isEmpty()
                || language.toLowerCase().equals("english"))
            return name;

        return localNamesMap.get(language);
    }
}
