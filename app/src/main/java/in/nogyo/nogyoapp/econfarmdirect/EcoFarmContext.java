package in.nogyo.nogyoapp.econfarmdirect;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.econfarmdirect.models.City;
import in.nogyo.nogyoapp.econfarmdirect.models.Order;
import in.nogyo.nogyoapp.econfarmdirect.models.ProductPricing;
import in.nogyo.nogyoapp.econfarmdirect.models.Store;
import in.nogyo.nogyoapp.econfarmdirect.models.User;
import in.nogyo.nogyoapp.econfarmdirect.rest.RestUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 21/05/16.
 */

@Getter @Setter @NoArgsConstructor
public class EcoFarmContext extends Application {

    private Store store;
    private City storeCity;
    private User sessionUser;
    private Order selectedOrder;
    private Map<Long,ProductPricing> cart = new HashMap<>();
    private String selectedLanguage;
    private boolean launchPlayStore;

    public void initializeCart(Activity activity){

        try{
            Map<String,String> data = (Map<String,String>)activity.getSharedPreferences(Constants.CART, Context.MODE_PRIVATE).getAll();
            String cartData = data.get(Constants.CART);
            if(cartData == null) return;
            this.cart = RestUtil.mapper.readValue(cartData, new TypeReference<Map<Long, ProductPricing>>() {
            });
        }catch (Exception e){
            Log.e("ERROR","Unable to initlialise the cart",e);
            this.cart = new HashMap<>();
        }
    }


    public void persistCart(){
        SharedPreferences sharedpreferences = getSharedPreferences(Constants.CART, Context.MODE_PRIVATE);
        if(this.getCart() != null){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            try {
                editor.putString(Constants.CART, RestUtil.mapper.writeValueAsString(this.getCart()));
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void addToCart(ProductPricing pricing){
        this.cart.put(pricing.getId(),pricing);
        this.persistCart();
    }

    public void removeFromCart(ProductPricing pricing){
        this.cart.remove(pricing.getId());
        this.persistCart();
    }
}
