package in.nogyo.nogyoapp.econfarmdirect.rest;


import java.net.HttpURLConnection;

/**
 * Created by srinand.pc on 21/02/16.
 */
public enum HttpConnectionBuilder {
    POST {
        @Override
        public void setRequestMethod(HttpURLConnection connection)  throws Exception {
            connection.setRequestMethod("POST");
        }
    },

    PUT {
        @Override
        public void setRequestMethod(HttpURLConnection connection)  throws Exception {
            connection.setRequestMethod("PUT");
        }
    },

    DELETE {
        @Override
        public void setRequestMethod(HttpURLConnection connection)  throws Exception {
            connection.setRequestMethod("DELETE");
        }
    },

    GET {
        @Override
        public void setRequestMethod(HttpURLConnection connection)  throws Exception {
            connection.setRequestMethod("GET");
        }
    };

    public abstract void setRequestMethod(HttpURLConnection connection) throws Exception;
}
