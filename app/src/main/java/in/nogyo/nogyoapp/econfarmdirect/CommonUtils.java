package in.nogyo.nogyoapp.econfarmdirect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by srinandchallur on 22/05/16.
 */
public class CommonUtils {


    public enum Languages{
        ENGLISH,TAMIL,TELUGU,KANNADA
    }

    public static Intent navigate(Activity currentActivity, Class newActivityClass){
        return new Intent(currentActivity, newActivityClass);
    }

    public static void navigateTo(Activity currentActivity, Class newActivityClass){
        EcoFarmContext context = (EcoFarmContext) currentActivity.getApplicationContext();
        currentActivity.startActivity(new Intent(currentActivity, newActivityClass));
    }

    public static void clearSessionData(Activity activity){
        SharedPreferences.Editor editor = activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
    }

    public static void blockThisThread(Long time){

        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            Log.e("ERROR", "ERROR in thread wait");
        }
    }


    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            Log.e("ERROR", "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride, e);
        }

    }

    public static String getLanguage(int index){
        return Languages.values()[index].toString().toLowerCase();

    }

    public static void setImage(ImageView imageView, String url ){

        BufferedInputStream bis = null;
        try{
            URL urlObject = new URL(url);
            URLConnection conn = urlObject.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            bis = new BufferedInputStream(is);
            Bitmap bmp = BitmapFactory.decodeStream(bis);
            imageView.setImageBitmap(bmp);
            bis.close();
            is.close();
            imageView.invalidate();
        }catch (Exception e){
            Log.e("ERROR", "Can not download image from: "+url);
        }

        finally {

        }
    }
}
