package in.nogyo.nogyoapp.econfarmdirect.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 25/02/16.
 */
@Getter @Setter @NoArgsConstructor
public class Order extends AbstractTimeStamp {

    private Long id;
    private Long userId;
    private List<OrderItem> orderItems;
    private String status;
    private String channel;
    private Date orderDate;
    private Long storeId;
    private Store store;
    private String fulfillmentStatus;
    private String returnStatus;
    private Double pricePromised;
    private Double priceCalculated;
    private Double discount;
    private List<OrderPayment> orderPayment;


    public OrderPayment getOrderPayment(){

        if(orderPayment != null && orderPayment.size() > 0)
            return orderPayment.get(0);

        return null;
    }

    public void setOrderPayment(OrderPayment orderPayment){
        if(this.orderPayment == null){
            this.orderPayment = new ArrayList<OrderPayment>();
        } else if(this.orderPayment.size() > 0){
            this.orderPayment.remove(0);
        }
        this.orderPayment.add(orderPayment);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", userId=" + userId +
                ", orderItems=" + orderItems +
                ", status='" + status + '\'' +
                ", channel='" + channel + '\'' +
                ", orderDate=" + orderDate +
                ", storeId=" + storeId +
                ", store=" + store +
                ", fulfillmentStatus='" + fulfillmentStatus + '\'' +
                ", returnStatus='" + returnStatus + '\'' +
                ", pricePromised=" + pricePromised +
                ", priceCalculated=" + priceCalculated +
                ", discount=" + discount +
                ", orderPayment=" + orderPayment +
                '}';
    }
}
