package in.nogyo.nogyoapp.econfarmdirect.models;

import org.codehaus.jackson.annotate.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 21/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class User extends AbstractTimeStamp {

    public enum UserStatus {INACTIVE, ACTIVE, DELETED}
    public enum UserType {SALES, ADMIN, STORE}

    private Long id;
    private String name;
    private String type;
    private String emailId;
    private String phoneNo;
    private String password;
    private String address;
    private String status;
    private String sessionId;
    private UserWallet userWallet;


    @JsonIgnore
    public boolean isSalesPerson(){
        return this.type.equals(UserType.SALES.toString());
    }

    @JsonIgnore
    public boolean isAdmin(){
        return this.type.equals(UserType.ADMIN.toString());
    }

    @JsonIgnore
    public boolean isStore(){
        return this.type.equals(UserType.STORE.toString());
    }
}
